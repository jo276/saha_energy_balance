/* Saha Ionization Balance Implementation */

/* written by JO, August 2016 */

#include <stdio.h>
#include "saha.h"

void main()
{
	/* This is the main wrapper function to run the Saha energy balance */
	/* Where explicitly noted everything is done in cgs units */
	/* I Assume the gas is non-degenerate and non-relativistic - Cox & Giuli Section 15*/

	double Pgas=1.01e5; /* Use this gas pressure */
	double Tgas=5779;   /* Use this gas temperature */

	double ne;
	ne=get_ne(Pgas,Tgas);

	printf("%e\n",ne);
}


double get_ne(double P, double T)
{
	/* Top level function that calculates electron density for given P & T */

	double ne = 0.; /* Initialise electron density */





	return ne;
}

double get_K(int i, int r, double P, double T);
{
	/* This function returns the 'K' function see 14.36 of KWW book. */


	/* first get ionization potential to level r+1 */
	chi_i_r=get_ionization_pot(i,r); 
	/* Now get degeneracies of level r and r+1, following KWW, use ground state values */	
	 



double get_ionization_pot(int i, int r);
{
	/* This function returns the ionization potentail from data structure */
	/* data is stored in electron volts, returned in cgs */

	ip=13.59844*EV; /* Just Hydrogen for Now */

	return ip;
}