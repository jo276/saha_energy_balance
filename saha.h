/* Header file for saha energy balance, written by JO, August 2016 */

/* some constants */

const double KB = 1.3806485e-16;
const double M_AMU = 1.66054e-24;
const double M_ELE = 9.10938e-28;
const double PI = 3.14159265359;
const double H_PLANK = 6.626070041e-27;
const double EV = 1.6021772e-12;


/* some functions */
double get_ne(double P, double T);